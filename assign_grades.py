# Import required modules
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as pat
from matplotlib.widgets import Slider
from matplotlib.patches import Rectangle
import pandas as pd
import os
import re
import seaborn as sb
from collections import OrderedDict
import sys

# Define the grades, GPA scale, and initial cutoffs
gpas = OrderedDict({'A+':9, 'A':8, 'A-':7, 'B+':6, 'B':5, 'B-':4, 'C+':3, 'C':2, 'C-':1, 'D':0, 'F':0})
cutoff_dict = OrderedDict({'A+':90, 'A':85, 'A-':80, 'B+':75, 'B':70, 'B-':65, 'C+':60, 'C':55, 'C-':50, \
        'D':0.1, 'F':0})
grades = np.array(list(cutoff_dict.keys()))
cutoffs = np.array([100] + list(cutoff_dict.values()), dtype=float)
cutoffs_off = cutoffs.copy()

# Load the initial cutoffs if they were previously defined
if os.path.exists('grade_cutoffs.csv'):
    df_cutoff = pd.read_csv('grade_cutoffs.csv')
    cutoffs = np.array([100] + list(df_cutoff['Cutoff']))
    cutoff_dict = {grade:cutoff for grade, cutoff in zip(grades, cutoffs[1:])}

# Define the grade colours
colours = np.array(sb.color_palette('muted'))[[0, 1, 2, 3, 4, 6, 7, 8, 5, 9, 0]]
colour_dict = {grade: colour for grade, colour in zip(grades, colours)}

# Import the student data and course totals exported from Learn
df = pd.read_csv('learn_export.csv', na_values='-').fillna(0)
df.rename({'Course total (Real)':'Score', 'ID number':'Student ID'}, axis=1, inplace=True)
df['Name'] = df.apply(lambda x: x['First name'] + ' ' + x['Surname'], axis=1)
num_students = len(df)

# Identify the students with zero scores
idx_zero = (df['Score'] == 0)
num_zeros = sum(idx_zero)

# Import the special considerations if any
if os.path.exists('special_considerations.csv'):
    df_spec = pd.read_csv('special_considerations.csv')
else:
    df_spec = pd.DataFrame({})
num_spec = len(df_spec)

# Assign the grades, accounting for any special considerations
df['Grade'] = df.apply(lambda x: grades[np.flatnonzero(cutoffs > x['Score'])[-1]], axis=1)
df['Grade unscaled'] = df.apply(lambda x: grades[np.flatnonzero(cutoffs_off > x['Score'])[-1]], axis=1)
df['Aegrotat'] = False
df['Special consideration reason'] = ''
df['Degree of impairment'] = ''
df['Grade adjustment'] = np.nan
df['Rationale for no grade adjustment'] = ''
idx_spec = np.zeros(num_students, dtype=bool)

for i in range(num_spec):
    idx = np.flatnonzero(df['Student ID'] == df_spec['Student ID'][i])
    if len(idx) == 1:
        idx = idx[0]
    else:
        print('Special consideration student ID {} not found in roster'.format(df_spec['Student ID'][i]))
        sys.exit(1)

    idx_spec[idx] = True
    df.loc[idx, 'Special consideration reason'] = df_spec['Special consideration reason'][i]
    df.loc[idx, 'Degree of impairment'] = df_spec['Degree of impairment'][i]
    df.loc[idx, 'Rationale for no grade adjustment'] = df_spec['Rationale for no grade adjustment'][i]

    idx_grade = np.flatnonzero(grades == df['Grade'][idx])[0]
    idx_grade_new = idx_grade - df_spec['Grade adjustment'][i]
    if idx_grade_new < 0:
        print('Invalid special consideration grade adjustment for {}'.format(dc_spec['Name'][i]))
        sys.exit(1)
    if df_spec['Grade adjustment'][i] > 0:
        df.loc[idx, 'Grade'] = grades[idx_grade_new]
        df.loc[idx, 'Aegrotat'] = True

# Plot a bar chart coloured according to the grade
idx_sort = np.argsort(df['Score'])
fig = plt.figure(figsize=(18, 10))
ax = fig.add_axes((0.05, 0.08, 0.80, 0.86))
bar_handles = []
bar_width = 0.5
for i in range(num_students - num_zeros):
    bar_handles += [ax.add_patch(Rectangle((i + 1 - bar_width/2.0, 0), bar_width, \
            df['Score'][idx_sort[num_zeros + i]], color=colour_dict[df['Grade'][idx_sort[num_zeros + i]]], \
            lw=0))]

# Compute and display the mean GPA
df['GPA'] = df.apply(lambda x: gpas[x['Grade']], axis=1)
df['GPA unscaled'] = df.apply(lambda x: gpas[x['Grade unscaled']], axis=1)
gpa_mean = np.mean(df['GPA'][~idx_zero])
gpa_unscaled_mean = np.mean(df['GPA unscaled'][~idx_zero])
gpa_handle = ax.set_title('Mean GPA: {:.2f}'.format(gpa_mean), fontsize='large')
print('Mean unscaled GPA: {:.2f}'.format(gpa_unscaled_mean))

# Plot the grade labels
line_handles = []
label_grade_handles = []
label_num_handles = []
start_grade = 0.003*(num_students - num_zeros)
start_num = 0.022*(num_students - num_zeros)
for grade in list(cutoff_dict)[:-2]:
    line_handles += ax.plot((0, sum(df['Score'] < cutoff_dict[grade]) - num_zeros + 0.3), \
            2*[cutoff_dict[grade]], color=colour_dict[grade], lw=0.5)
    label_grade_handles += [ax.text(start_grade, cutoff_dict[grade] + 0.5, '{}'.format(grade), \
            color=colour_dict[grade], fontsize='small', horizontalalignment='left')]
    label_num_handles += [ax.text(start_num, cutoff_dict[grade] + 0.5, ': {}'.format(sum(df['Grade'] == \
            grade)), color=colour_dict[grade], fontsize='small', horizontalalignment='left')]
label_grade_handles += [ax.text(start_grade, cutoff_dict['C-'] - 2.2, '{}'.format('D'), \
        color=colour_dict['D'], fontsize='small', horizontalalignment='left')]
label_num_handles += [ax.text(start_num, cutoff_dict['C-'] - 2.2, ': {}'.format(sum(df['Grade'] == 'D')), \
        color=colour_dict['D'], fontsize='small', horizontalalignment='left')]

# Define the slider update function
num_sliders = len(grades) - 2
def update(val):

    # Reassign the grades, accounting for any special considerations
    for i in range(num_sliders):
        cutoffs[i + 1] = sliders[i].val
    cutoff_dict = {grade: cutoff for grade, cutoff in zip(grades, cutoffs[1:])}
    df['Grade'] = df.apply(lambda x: grades[np.flatnonzero(cutoffs > x['Score'])[-1]], axis=1)
    for i in range(num_spec):
        idx = np.flatnonzero(df['Student ID'] == df_spec['Student ID'][i])[0]
        idx_grade = np.flatnonzero(grades == df['Grade'][idx])[0]
        idx_grade_new = idx_grade - df_spec['Grade adjustment'][i]
        if idx_grade_new < 0:
            print('Invalid special consideration grade adjustment for {}'.format(dc_spec['Name'][i]))
            sys.exit(1)
        if df_spec['Grade adjustment'][i] > 0:
            df.loc[idx, 'Grade'] = grades[idx_grade_new]

    # Update the bar colours
    for i in range(num_students - num_zeros):
        bar_handles[i].set_color(colour_dict[df['Grade'][idx_sort[num_zeros + i]]])

    # Recompute and update the mean GPA
    df['GPA'] = df.apply(lambda x: gpas[x['Grade']], axis=1)
    gpa_mean = np.mean(df['GPA'][~idx_zero])
    gpa_handle.set_text('Mean GPA: {:.2f}'.format(gpa_mean))

    # Update the labels
    for i in range(len(grades) - 2):
        line_handles[i].set_data((0, sum(df['Score'] < cutoff_dict[grades[i]]) - num_zeros + 0.3), \
            2*[cutoff_dict[grades[i]]])
        label_grade_handles[i].set_position((start_grade, cutoff_dict[grades[i]] + 0.5))
        label_grade_handles[i].set_text('{}'.format(grades[i]))
        label_num_handles[i].set_position((start_num, cutoff_dict[grades[i]] + 0.5))
        label_num_handles[i].set_text(': {}'.format(sum(df['Grade'] == grades[i])))
    label_grade_handles[-1].set_position((start_grade, cutoff_dict['C-'] - 2.2))
    label_grade_handles[-1].set_text('{}'.format('D'))
    label_num_handles[-1].set_position((start_num, cutoff_dict['C-'] - 2.2))
    label_num_handles[-1].set_text(': {}'.format(sum(df['Grade'] == 'D')))

    fig.canvas.draw_idle()

# Create the sliders
start = 0.86
end = 0.99
spacing = (end - start)/num_sliders
width = 0.7*spacing
sliders = []
for i in range(num_sliders):
    slider_axes = fig.add_axes((start + (num_sliders - 1 - i)*spacing, 0.08, width, 0.86))
    slider = Slider(ax=slider_axes, label=grades[i], valmin=0, valmax=100, valinit=cutoff_dict[grades[i]], \
            valfmt='%.1f', valstep=0.1, orientation='vertical', color=colour_dict[grades[i]])
    slider.label.set_size('small')
    slider.valtext.set_size('x-small')
    slider.valtext.set_rotation(45)
    slider.valtext.set_horizontalalignment('right')
    slider.valtext.set_x(1)
    slider.on_changed(update)
    sliders += [slider]

# Link the sliders to avoid one overshooting the other
for i in range(num_sliders):
    if i > 0:
        sliders[i].slidermax = sliders[i - 1]
    if i < num_sliders - 1:
        sliders[i].slidermin = sliders[i + 1]

## Plot the student names (makes the plot updates lag when the sliders are pulled)
#ax.set_xlim(0, num_students - num_zeros + 0.5)
#ax.set_xticks(np.arange(1, num_students - num_zeros + 1) + 0.5)
#ax.xaxis.set_tick_params(length=0)
#ax.set_xticklabels(df['Name'][idx_sort][num_zeros:], rotation=45, horizontalalignment='right', fontsize=4)
#for i, label in enumerate(ax.get_xticklabels()):
#    label.set_color(colour_dict[df['Grade'][idx_sort[num_zeros + i]]])

# Finalise the plot
ax.set_xlim(0, num_students - num_zeros + 0.5)
ax.set_ylim(0, 100)
ax.set_yticks(np.arange(0, 105, 10))
ax.set_xlabel('Student number')
ax.set_ylabel('Total course score')

fig.savefig('grades_distribution.png')
plt.show()
plt.close()

# Save the final cutoffs
df_cutoff = pd.DataFrame({'Grade':grades, 'Cutoff':cutoffs[1:]})
df_cutoff.to_csv('grade_cutoffs.csv', float_format='%.1f', index=False)

# Save the percentages and grades in descending order of rank
df['Grade with Aegrotats'] = df.apply(lambda x: x['Grade'] + ' Aeg' if x['Aegrotat'] else x['Grade'], axis=1)
df.sort_values('Score', ascending=False, inplace=True)
df.to_csv('../scores_grades_sorted.csv', columns=('Name', 'Student ID', 'Email address', 'Score', \
        'Grade with Aegrotats', 'Grade unscaled', 'Special consideration reason', 'Degree of impairment', \
        'Rationale for no grade adjustment'), index=False)
